from core.entities import User
from core.repos import IUsersRepo
from database.config import session
from database.tables import UserTable


class UsersRepo(IUsersRepo):

    def register_user(self, telegram_chat_id: str, redmine_user_name: str) -> User:
        session.begin()
        user_table = UserTable(telegram_chat_id=telegram_chat_id, redmine_user_name=redmine_user_name)
        session.add(user_table)
        session.commit()
        return User(telegram_chat_id=telegram_chat_id, redmine_user_name=redmine_user_name)

    def get_user_by_telegram_chat_id(self, telegram_chat_id: str) -> User:
        user_table = session.query(UserTable).get(telegram_chat_id=telegram_chat_id)
        return User(telegram_chat_id=telegram_chat_id, redmine_user_name=user_table.redmine_user_name)

    def get_user_by_redmine_user_name(self, redmine_user_name: str) -> User:
        user_table = session.query(UserTable).get(redmine_user_name=redmine_user_name)
        return User(telegram_chat_id=user_table.telegram_chat_id, redmine_user_name=user_table.redmine_user_name)

    def get_all_users(self) -> list[User]:
        user_tables = session.query(UserTable).all()
        return [
            User(telegram_chat_id=user_table.telegram_chat_id, redmine_user_name=user_table.redmine_user_name)
            for user_table in user_tables
        ]


