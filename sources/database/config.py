
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


db_engine = create_engine("postgresql://root:root@localhost:5432/db")

PsqlSession = sessionmaker(bind=db_engine, autoflush=True, autocommit=True)

session = PsqlSession()
