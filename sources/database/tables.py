from sqlalchemy import Column, String, Integer

from database.base import BaseTable


class UserTable(BaseTable):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    telegram_chat_id = Column(String, unique=True)
    redmine_user_name = Column(String, unique=True)
