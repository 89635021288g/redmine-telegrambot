from core.repos import IUsersRepo
from database.repos import UsersRepo


class Service:
    repo: IUsersRepo = UsersRepo()

    def register_user(self, telegram_chat_id: str, redmine_user_name: str):
        user = self.repo.register_user(telegram_chat_id, redmine_user_name)
        return dict(
            telegram_chat_id=user.telegram_chat_id,
            redmine_user_name=user.telegram_chat_id,
        )

    def get_user_by_telegram_chat_id(self, telegram_chat_id: str):
        user = self.repo.get_user_by_telegram_chat_id(telegram_chat_id)
        return dict(
            telegram_chat_id=user.telegram_chat_id,
            redmine_user_name=user.telegram_chat_id,
        )

    def get_user_by_redmine_user_name(self, redmine_user_name: str):
        user = self.repo.get_user_by_redmine_user_name(redmine_user_name)
        return dict(
            telegram_chat_id=user.telegram_chat_id,
            redmine_user_name=user.telegram_chat_id,
        )

    def get_all_users(self):
        users = self.repo.get_all_users()
        return [
            dict(
                telegram_chat_id=user.telegram_chat_id,
                redmine_user_name=user.telegram_chat_id,
            ) for user in users
        ]
