from abc import ABC, abstractmethod

from core.entities import User


class IUsersRepo(ABC):
    @abstractmethod
    def register_user(self, telegram_chat_id: str, redmine_user_name: str) -> User: ...

    @abstractmethod
    def get_user_by_telegram_chat_id(self, telegram_chat_id: str) -> User: ...

    @abstractmethod
    def get_user_by_redmine_user_name(self, redmine_user_name: str) -> User: ...

    @abstractmethod
    def get_all_users(self) -> list[User]: ...
