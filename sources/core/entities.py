from dataclasses import dataclass


@dataclass
class User:
    telegram_chat_id: str
    redmine_user_name: str
