from flask import Flask

from api.urls import register_urls

app = Flask(__name__)
register_urls(app)