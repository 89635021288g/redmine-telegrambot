from flask import Flask as Flask
from . import views


def register_urls(app: Flask):
    app.add_url_rule('/hook/', view_func=views.hook, methods=["POST"])
